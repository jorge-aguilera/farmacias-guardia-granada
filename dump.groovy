@Grab(group='io.github.http-builder-ng', module='http-builder-ng-apache', version='1.0.3')
@Grab('org.jsoup:jsoup:1.9.2')

import static groovyx.net.http.HttpBuilder.configure
import org.jsoup.*
import org.jsoup.nodes.*
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.OutputStream;

// Create a trust manager that does not validate certificate chains like the default

TrustManager[] trustAllCerts = [
        new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers()
            {
                return null;
            }
            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
            {
                //No need to implement.
            }
            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
            {
                //No need to implement.
            }
        }
]
// Install the all-trusting trust manager
try
{
    SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
}
catch (Exception e)
{
    System.out.println(e);
}

def http = configure {
    request.uri = 'https://www.cofgranada.com'
}

localidades = [
[localidad:"Agrón",mlat:37.03,mlong: -3.829167],
[localidad:"Alamedilla",mlat:37.581667,mlong:-3.244444],
[localidad:"Albolote",mlat:37.230556,mlong:-3.657222],
[localidad:"Albondón",mlat:36.82729,mlong:-3.210874],
[localidad:"Albuñán",mlat:37.227082,mlong:-3.133197],
[localidad:"Albuñol",mlat:36.791389,mlong:-3.203333],
[localidad:"Albuñuelas",mlat:36.928056,mlong:-3.631667],
[localidad:"Alcudia de Guadix",mlat:37.257597,mlong: -3.097447],
[localidad:"Aldeire",mlat:37.160375,mlong:-3.071595],
[localidad:"Alfacar",mlat:37.236667,mlong: -3.570833],
[localidad:"Algarinejo",mlat:37.325,mlong:-4.158611],
[localidad:"Alhama de Granada",mlat:37.002555,mlong:-3.988069],
[localidad:"Alhendín",mlat:37.107778,mlong:-3.645833],
[localidad:"Alicún de Ortega",mlat:37.608056,mlong:-3.1375],
[localidad:"Almegíjar",mlat:36.903015,mlong:-3.300126],
[localidad:"Almuñécar",mlat:36.733889,mlong:-3.691111],
[localidad:"Alomartes",mlat:37.261667,mlong:-3.912222],
[localidad:"Alquife",mlat:37.179956,mlong:-3.115922],
[localidad:"Ambroz",mlat:37.164167,mlong:-3.663611],
[localidad:"Arenas del Rey",mlat:36.957778,mlong: -3.894167],
[localidad:"Arenas del Rey - FORNES",mlat:36.954444,mlong:-3.855278],
[localidad:"Arenas del Rey - JÁTAR",mlat:36.934722,mlong:-3.91],
[localidad:"Armilla",mlat:37.142778,mlong: -3.627778],
[localidad:"Atarfe",mlat:37.222778,mlong:-3.686389],
[localidad:"Bácor-Olivar",mlat:37.516667,mlong:-2.95],
[localidad:"Baza",mlat:37.488864,mlong:-2.770981],
[localidad:"Beas de Granada",mlat:37.218611,mlong:-3.481111],
[localidad:"Beas de Guadix",mlat:37.27967,mlong: -3.205717],
[localidad:"Belicena",mlat:37.170833,mlong:-3.692222],
[localidad:"Benalúa de Guadix",mlat:37.349722,mlong:-3.168611],
[localidad:"Benalúa de las Villas",mlat:37.431667,mlong:-3.6825],
[localidad:"Benamaurel",mlat:37.608353,mlong:-2.697231],
[localidad:"Bérchules",mlat:36.9753604,mlong:-3.1900552],
[localidad:"Bubión",mlat:36.9491316,mlong:-3.3561111],
[localidad:"Busquístar",mlat:36.9375,mlong:-3.2944444444444],
[localidad:"Cacín",mlat:37.06,mlong:-3.9169444444444],
[localidad:"Cádiar",mlat:36.946111111111,mlong:-3.1802777777778],
[localidad:"Cájar",mlat:37.134166666667,mlong:-3.5708333333333],
[localidad:"Calahonda",mlat:36.700833333333,mlong:-3.4161111111111],
[localidad:"Calahorra, La",mlat:37.179444444444,mlong:-3.0622222222222],
[localidad:"Calicasas",mlat:37.273333333333,mlong:-3.6186111111111],
[localidad:"Campo Cámara",mlat:37.736111111111,mlong:-2.8619444444444],
[localidad:"Campotéjar",mlat:37.481388888889,mlong:-3.6166666666667],
[localidad:"Caniles",mlat:37.4341991,mlong:-2.7245038],
[localidad:"Cáñar",mlat:36.926388888889,mlong:-3.4277777777778],
[localidad:"Capileira",mlat:36.961388888889,mlong:-3.3586111111111],
[localidad:"Carataunas",mlat:36.9231116,mlong:-3.4087569],
[localidad:"Carchuna",mlat:36.701944444444,mlong:-3.4419444444444],
[localidad:"Cástaras",mlat:36.931388888889,mlong:-3.2536111111111],
[localidad:"Castell de Ferro",mlat:36.7225,mlong:-3.3591666666667],
[localidad:"Castilléjar",mlat:37.714722222222,mlong:-2.6430555555556],
[localidad:"Castril",mlat:37.795833333333,mlong:-2.7788888888889],
[localidad:"Cenes de la Vega",mlat:37.159444444444,mlong:-3.5386111111111],
[localidad:"Chaparral, El",mlat:37.2575,,mlong:-3.655833],
[localidad:"Chauchina",mlat:37.201389,mlong:-33.7725],
[localidad:"Chimeneas",mlat:37.131247,mlong:-3.823602],
[localidad:"Churriana de la Vega",mlat:37.147778,mlong:-3.646111],
[localidad:"Cijuela",mlat:37.2,mlong:-3.810556],
[localidad:"Cogollos de Guadix",mlat:37.224444,mlong:-3.160833],
[localidad:"Cogollos Vega",mlat:37.274722,mlong:-3.573056],
[localidad:"Colomera",mlat:37.371667,mlong:-3.714167],
[localidad:"Cortes de Baza",mlat:37.654419,mlong:-2.770186],
[localidad:"Cortes y Graena",mlat:37.303611,mlong:-3.218889],
[localidad:"Cozvijar",mlat:36.990278,mlong:-3.588889],
[localidad:"Cuevas del Campo",mlat:37.606791,mlong:-2.929068],
[localidad:"Cúllar",mlat:37.583611,mlong:-2.576389],
[localidad:"Cúllar Vega",mlat:37.153056,mlong:-3.670556],
[localidad:"Cúllar Vega - EL VENTORRILLO",mlat:-37.160556,mlong:-3.691667],
[localidad:"Darro",mlat:37.349444,mlong:-3.293889],
[localidad:"Dehesas de Guadix",mlat:37.59,mlong:-3.102778],
[localidad:"Dehesas Viejas",mlat:0,mlong:0],
[localidad:"Deifontes",mlat:37.326389,mlong:-3.594444],
[localidad:"Diezma",mlat:37.320833,mlong:-3.331667],
[localidad:"Dílar",mlat:37.076111,mlong:-3.602222],
[localidad:"Dólar",mlat:37.18,mlong:-2.989722],
[localidad:"Domingo Perez",mlat:37.497222,mlong:-3.508333],
[localidad:"Dúdar",mlat:37.185833,mlong:-3.484722],
[localidad:"Dúrcal",mlat:36.98766,mlong:-3.565898],
[localidad:"Escoznar",mlat:-37.236944,mlong:-3.846389],
[localidad:"Escúzar",mlat:37.061944,mlong:-3.761111],
[localidad:"Ferreira",mlat:37.172222,mlong:-3.036111],
[localidad:"Fonelas",mlat:37.4125,mlong:-3.176111],
[localidad:"Freila",mlat:37.528725,mlong:-2.908098],
[localidad:"Fuente Vaqueros",mlat:37.219444,mlong:-3.783056],
[localidad:"Gabias, Las",mlat:37.136389,mlong:-3.669167],
[localidad:"Galera",mlat:37.743056,mlong:-2.551389],
[localidad:"Gobernador",mlat:0,mlong:0],
[localidad:"Gójar",mlat:0,mlong:0],
[localidad:"Gor",mlat:0,mlong:0],
[localidad:"Gorafe",mlat:0,mlong:0],
[localidad:"Granada",mlat:0,mlong:0],
[localidad:"Guadahortuna",mlat:0,mlong:0],
[localidad:"Guadix",mlat:0,mlong:0],
[localidad:"Guejar Faragüit",mlat:0,mlong:0],
[localidad:"Güéjar Sierra",mlat:0,mlong:0],
[localidad:"Güevéjar",mlat:0,mlong:0],
[localidad:"Herradura, La",mlat:0,mlong:0],
[localidad:"Hijar",mlat:0,mlong:0],
[localidad:"Huélago",mlat:0,mlong:0],
[localidad:"Huéneja",mlat:0,mlong:0],
[localidad:"Huéscar",mlat:0,mlong:0],
[localidad:"Huétor de Santillán",mlat:0,mlong:0],
[localidad:"Huétor Tájar",mlat:0,mlong:0],
[localidad:"Huétor Vega",mlat:0,mlong:0],
[localidad:"Illora",mlat:0,mlong:0],
[localidad:"Itrabo",mlat:0,mlong:0],
[localidad:"Iznalloz",mlat:0,mlong:0],
[localidad:"Jayena",mlat:0,mlong:0],
[localidad:"Jerez del Marquesado",mlat:0,mlong:0],
[localidad:"Jete",mlat:0,mlong:0],
[localidad:"Jun",mlat:0,mlong:0],
[localidad:"Juviles",mlat:0,mlong:0],
[localidad:"Láchar",mlat:0,mlong:0],
[localidad:"Lanjarón",mlat:0,mlong:0],
[localidad:"Lanteira",mlat:0,mlong:0],
[localidad:"Laroles",mlat:0,mlong:0],
[localidad:"Lecrín",mlat:0,mlong:0],
[localidad:"Lentegí",mlat:0,mlong:0],
[localidad:"Lobras/Juviles",mlat:0,mlong:0],
[localidad:"Loja",mlat:0,mlong:0],
[localidad:"Lugros",mlat:0,mlong:0],
[localidad:"Lújar",mlat:0,mlong:0],
[localidad:"Malahá, La",mlat:0,mlong:0],
[localidad:"Mamola, La",mlat:0,mlong:0],
[localidad:"Maracena",mlat:0,mlong:0],
[localidad:"Marchal",mlat:0,mlong:0],
[localidad:"Mecina Bombaron",mlat:0,mlong:0],
[localidad:"Moclín - OLIVARES",mlat:0,mlong:0],
[localidad:"Moclín - PUERTO LOPE",mlat:0,mlong:0],
[localidad:"Moclín - TIENA",mlat:0,mlong:0],
[localidad:"Molvízar",mlat:0,mlong:0],
[localidad:"Monachil",mlat:0,mlong:0],
[localidad:"Monachil-SIERRA NEVADA",mlat:0,mlong:0],
[localidad:"Montefrío",mlat:0,mlong:0],
[localidad:"Montejícar",mlat:0,mlong:0],
[localidad:"Montillana",mlat:0,mlong:0],
[localidad:"Moraleda de Zafayona",mlat:0,mlong:0],
[localidad:"Moraleda de Zafayona -LORETO",mlat:0,mlong:0],
[localidad:"Morelábor",mlat:0,mlong:0],
[localidad:"Motril",mlat:0,mlong:0],
[localidad:"Murtas",mlat:0,mlong:0],
[localidad:"Nigüelas",mlat:0,mlong:0],
[localidad:"Nívar",mlat:0,mlong:0],
[localidad:"Ogíjares",mlat:0,mlong:0],
[localidad:"Orce",mlat:0,mlong:0],
[localidad:"Órgiva",mlat:0,mlong:0],
[localidad:"Otívar",mlat:0,mlong:0],
[localidad:"Otura",mlat:0,mlong:0],
[localidad:"Padul",mlat:0,mlong:0],
[localidad:"Pampaneira",mlat:0,mlong:0],
[localidad:"Pedro Martínez",mlat:0,mlong:0],
[localidad:"Peligros",mlat:0,mlong:0],
[localidad:"Peza, La",mlat:0,mlong:0],
[localidad:"Pinos del Valle",mlat:0,mlong:0],
[localidad:"Pinos Genil",mlat:0,mlong:0],
[localidad:"Pinos Puente",mlat:0,mlong:0],
[localidad:"Piñar",mlat:0,mlong:0],
[localidad:"Pitres",mlat:0,mlong:0],
[localidad:"Polícar",mlat:0,mlong:0],
[localidad:"Pórtugos",mlat:0,mlong:0],
[localidad:"Puebla de Don Fadrique",mlat:0,mlong:0],
[localidad:"Pulianas",mlat:0,mlong:0],
[localidad:"Pulianillas",mlat:0,mlong:0],
[localidad:"Purchil",mlat:0,mlong:0],
[localidad:"Purullena",mlat:0,mlong:0],
[localidad:"Quéntar",mlat:0,mlong:0],
[localidad:"Rábita, La",mlat:0,mlong:0],
[localidad:"Restabal",mlat:0,mlong:0],
[localidad:"Rubite",mlat:0,mlong:0],
[localidad:"Salar",mlat:0,mlong:0],
[localidad:"Salobreña",mlat:0,mlong:0],
[localidad:"Santa Cruz del Comercio",mlat:0,mlong:0],
[localidad:"Santa Fe",mlat:0,mlong:0],
[localidad:"Soportújar",mlat:0,mlong:0],
[localidad:"Sorvilán",mlat:0,mlong:0],
[localidad:"Tocon",mlat:0,mlong:0],
[localidad:"Torre-Cardela",mlat:0,mlong:0],
[localidad:"Torrenueva",mlat:0,mlong:0],
[localidad:"Torvizcón",mlat:0,mlong:0],
[localidad:"Trevélez",mlat:0,mlong:0],
[localidad:"Turón",mlat:0,mlong:0],
[localidad:"Ugíjar",mlat:0,mlong:0],
[localidad:"Valderrubio",mlat:0,mlong:0],
[localidad:"Válor",mlat:0,mlong:0],
[localidad:"Varadero - El Puerto (Motril)",mlat:0,mlong:0],
[localidad:"Vélez de Benaudalla",mlat:0,mlong:0],
[localidad:"Ventas de Huelma",mlat:0,mlong:0],
[localidad:"Ventas de Zafarraya",mlat:0,mlong:0],
[localidad:"Ventorros de San José",mlat:0,mlong:0],
[localidad:"Villanueva de las Torres",mlat:0,mlong:0],
[localidad:"Villanueva Mesía",mlat:0,mlong:0],
[localidad:"Viznar",mlat:0,mlong:0],
[localidad:"Zafarraya",mlat:0,mlong:0],
[localidad:"Zagra",mlat:0,mlong:0],
[localidad:"Zubia, La",mlat:0,mlong:0],
[localidad:"Zujaira-Casanueva",mlat:0,mlong:0],
[localidad:"Zújar",mlat:0,mlong:0],
]

index = new File("docs/modules/ROOT/pages/index.adoc")
index.parentFile.mkdirs()
index.text = new File("index.adoc").text
index << "\n\nÚltima actualización ${new Date().format('yyyy-MM-dd HH:mm')}\n"

nav = new File('docs/modules/ROOT/nav.adoc')
nav.parentFile.mkdirs()
nav.text = ""

geoposicion = new File('estaciones.json')
geoposicion.text = "["

today = new Date().format('dd/MM/yyyy')
localidades.eachWithIndex { entry , key->
    localidad = entry.localidad
    page = http.post {
        request.uri.path = '/paginas/Farmacias-Guardia-Resultado.asp'
        request.body = [localidad: localidad, fecha: today, Enviar: 'Enviar']
        request.contentType = 'application/x-www-form-urlencoded'
    }
    campos = []
    page.select('div.BuscadorResultadoCampo').each {
        campos << it.text()
    }
    resultados = []
    page.select('div.BuscadorResultadoTexto').each {
        resultados << it.text()
    }

    if( campos.size() ) {
        file = new File("docs/modules/ROOT/pages/${key}.adoc")
        file.parentFile.mkdirs()
        file.text = "= $localidad\n:tabs:\n\n"

        file << "https://www.openstreetmap.org/?mlat=$entry.mlat&mlon=$entry.mlong#map=17/$entry.mlat/$entry.mlong[Ver en mapa,window=_blank]\n\n"

        file << '|===\n'
        campos.eachWithIndex { ele, idx ->
            file << "| $ele "
        }
        file << "\n\n"

        resultados.eachWithIndex { ele, idx ->
            file << "| $ele "
        }
        file << '\n|===\n\n'

        nav << "* xref:${key}.adoc[$localidad]\n"

        geoposicion << """{"link":"${key}.html","mlat":$entry.mlat,"mlon":$entry.mlong},\n"""
    }
}
geoposicion << """{"link":"index.html","mlat":0,"mlon":0}]"""

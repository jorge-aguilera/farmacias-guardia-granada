@Grab(group='io.github.http-builder-ng', module='http-builder-ng-apache', version='1.0.3')
@Grab('org.jsoup:jsoup:1.9.2')

import static groovyx.net.http.HttpBuilder.configure
import org.jsoup.*
import org.jsoup.nodes.*
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.OutputStream;

def http = configure {
    request.uri = 'https://es.wikipedia.org'
}

args.each{ city ->
    page = http.get{
        request.uri.path = "/wiki/${city}"
    }
    geo = page.select('.geo-default').first()
    if( geo ){
        href = geo.parent().attr('href')
        myMatcher = href =~ /.+&params=([-0-9\.]+)_N_([-0-9\.]+)/
        myMatcher.find()
        lat = myMatcher[0][1]
        lng =  myMatcher[0][2]
        println """ [localidad:"$city",mlat:$lat,mlong:$lng],"""
    }
}